\documentclass[12pt]{article}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{filecontents}
\usepackage[style=verbose]{biblatex}
\graphicspath{{images/}}

\begin{filecontents}{projectbib.bib}
@online {miconi057729,
	author = {Miconi, Thomas},
	title = {Biologically plausible learning in recurrent neural networks for flexible decision tasks},
	year = {2016},
	doi = {10.1101/057729},
	publisher = {Cold Spring Harbor Laboratory},
	abstract = {Recurrent neural networks operating in the near-chaotic regime exhibit complex dynamics, reminiscent of neural activity in higher cortical areas. As a result, these networks have been proposed as models of cortical computation during cognitive tasks. However, existing methods for training the connectivity of these networks are either biologically implausible, and/or require an instantaneous, real-time continuous error signal to guide the learning process. The lack of plausible learning method may restrict the applicability of recurrent neural networks as models of cortical computation. Here we introduce a biologically plausible learning rule that can train such recurrent networks, guided solely by delayed, phasic rewards at the end of each trial. We use this method to learn various tasks from the experimental literature, showing that this learning rule can successfully implement flexible associations, memory maintenance, nonlinear mixed selectivities, and coordination among multiple outputs. The trained networks exhibit complex dynamics previously observed in animal cortex, such as dynamic encoding and maintenance of task features, switching from stimulus-specific to response-specific representations, and selective integration of relevant input streams. We conclude that recurrent neural networks can offer a plausible model of cortical dynamics during both learning and performance of flexible behavior.},
	URL = {https://www.biorxiv.org/content/early/2016/06/07/057729},
	eprint = {https://www.biorxiv.org/content/early/2016/06/07/057729.full.pdf},
	journal = {bioRxiv}
}
\end{filecontents}

\addbibresource{projectbib.bib}

\newcommand{\rbar}
{
	\overline{R}
}

\newcommand{\xbar}
{
	\overline{x}
}

\newcommand{\E}
{
	\mathbb{E}
}

\begin{document}
\title{Biologically Plausible Learning\\ in a Recurrent Neural Network}
\author{Nicholas Gyd\'e}
\maketitle

\section{Overview}

This project is based on the paper \textit{Biologically Plausible Learning in Recurrent Neural Networks for Flexible Decision Tasks}.\footnote{Thomas Miconi, \textit{Biologically Plausible Learning in Recurrent Neural Networks for Flexible Decision Tasks}, bioRxiv (2016)} In it, Thomas Miconi presents a recurrent neural network learning algorithm that is trained only by delayed, global reward signals. His model is biologically plausible because it doesn't require omniscient knowledge (such as a high-dimensional error gradient), succeeds with non-instantaneous feedback, and uses a global reward signal comparable to what an animal might experience when succeeding or failing at a general task.

Learning is done by accumulating would-be Hebbian weight changes over the course of a trial, then applying them all at once when the reward or punishment is received. Specifically, the "eligibility" $e$ for a future weight change in the synapse from neuron $i$ to neuron $j$ is accumulated by the discrete-time rule:
\begin{equation}
e_{i,j}(t) = e_{i,j}(t-1) + r_i(t-1) * S(x_j(t) - \xbar)
\end{equation}
where $\xbar$ represents an exponential average of $x$ (i.e. $\xbar(t) = \alpha_x\xbar(t-1) + (1-\alpha_x)x(t)$ for some $\alpha_x$) and $S$ is some supraliniear function, suggested by Miconi to be $x^3$ or $x|x|$. We always used the former. 

When the reward signal is received, these eligibilities are used to update the weight matrix $J$ using one of the two following rules:

\begin{align}
&& \Delta J_{i,j} = \eta e_{i,j}\rbar(R - \rbar)\\
\text{or} && \Delta J_{i,j} = \eta e_{i,j} (R - \rbar)
\end{align}
$\rbar$ is an exponential average of recent $R$ for the current trial type (inputs), computed similarly to $\xbar$.
$\eta$ is the learning rate.

For this project we implemented a recurrent neural network and used Miconi's algorithm to teach it an XOR function on time-delayed inputs. We also experimented with a few variations in the implementation, including trying both learning rules (2) and (3).

\newpage
\section{Simulation}
The network has $N$ neurons, and takes a time-dependent vector of $M$ inputs $u(t)$. Matrices $B$ and $J$ specify input-to-neuron and neuron-to-neuron weights, respectively. Neuron $k$ is said to have excitation $x_k(t)$, and fire an output signal at a rate $r_k(t) = \tanh(x_k(t))$. This firing then influences the excitation of other neurons under the following rule
\begin{equation}
\tau \frac{\mathrm{d}x_i}{\mathrm{dt}} = -x_i(t) + \sum_{j=1}^N J_{j,i}r_j(t) + \sum_{k=1}^M B_{k,i}u_k(t)
\end{equation}

which we approximate in discrete time steps $\delta t$ using the Euler approximation
\begin{equation}
\delta x_i = \bigg( -x_i(t) + \sum_{j=1}^N J_{j,i}r_j(t) + \sum_{k=1}^M B_{k,i}u_k(t) \bigg) \frac{\delta t}{\tau}
\end{equation}

We occasionally perturb neurons' excitations by a small, random amount. The resulting variation in network output may result in sudden reward/punishment spikes, which are useful for creating nonzero weight updates. We also set aside a few neurons to fire at constant rates so others can learn to use them as biases. It is important not to perturb neurons being used as biases or network outputs.\\

Finally, after the network has run for a predetermined length of time, it is issued a scalar reward $R$. The reward is calculated based on the recent firing rates of a subset of neurons arbitrarily chosen to represent the network's output. We use the reward and our accumulated potential weight changes(eq (1)) to update $J$, using eq (1) or (2). We restrict the magnitude of these weight updates as a precaution.\\

In all experiments, we used the following parameters
\begin{align*}
N & = 200\\
\Delta J_{max} & = 3*10^{-4}\\
\eta & = 0.1\\
\delta t & = 1\\
\#Trials & = 10,000\\
f_{perturb} & = 3 \text{Hz}\\
\tau & = 30*\delta t\\
\alpha_x & = 0.05\\
\alpha_R & = 0.75\\
B_{i,j}(0) & \sim U([-1, 1])\\
J_{i,j}(0) & \sim \mathcal{N}(0, \frac{1.5^2}{N})\\
Perturbations & \sim U([-0.5, 0.5])
\end{align*}

\section{Results}
\subsection{Time-Delayed XOR using the First Update Rule}
In this experiment we had two input signals, one fired between timesteps 0 to 200, the other between 400 to 600. Each input signal is independently 1 or -1 during its interval. We selected a neuron $o$ as the output, and measured its response over timesteps 800 to 1000. After measuring the response, we issue a reward
$$
R = - \E\big( |r_o(t) - target| \big)
$$

Where the target is -1 if the inputs were equal, 1 otherwise. We then used this $R$ to update the network's weights using eq.(2) This task is difficult because the temporal separations from input to input to output are large compared to $\tau$, while the excitation decay term $-x_i(t)$ from eq.(4) only gives an excitation a mean lifetime of $\tau$. Thus the network cannot rely on latent excitations, but must learn to remember the inputs and make their comparison later. Additionally, the network has to learn the non-linearly-separable target function.

This experiment was successful, as it was in Miconi's paper. Here we can see how the output neuron responded to each input, both in an early trial (\#575), and after the last trial.
\begin{figure}[h]
\includegraphics[scale=0.5]{learning}
\end{figure}

By trial 575, the sign of the output was already correct (during the 200 step evaluation period), and the separation grew more pronounced throughout training, as we would hope. Below are the data after 20 runs of 10,000 trials each. $T_{success}$ represents the number of trials required before 95 consecutive trials had the correct output sign.
\newpage
\begin{verbatim}
T_success:
4265, 4698, 4233, 5374, 6558, 6953, 6131, 3929, 3292, 8080,
4598, 4156, 3218, 4001, 3912, 2147, 5595, 2467, 7624, 4651
Median T_success: 4431.5
25th percentile T_success: 3924.75
75th percentile T_success: 5729.0
Average -R after training: 0.252483285858
\end{verbatim}

\subsection{Time-Delayed XOR using the Second Update Rule}
We repeated the previous experiment, but used eq.(3) for weight updates.
The results below show that many runs were not able to stably converge, but
the fastest successful runs were faster than those of the previous experiment.
The extra $\rbar$ term in eq.(2) stabilized weight updates near the correct answer.
(where $\rbar = 0$ with our reward function)
\begin{verbatim}
T_success:
3975, 5001, 10000, 4416, 10000, 885, 6020, 9725, 9735, 2462,
9190, 8239, 6859, 10000, 2609, 8166, 2452, 4179, 2653, 2988
Median T_success: 5510.5
25th percentile T_success: 2904.25
75th percentile T_success: 9323.75
Average -R after training: 0.215698502274
\end{verbatim}

The figure below shows $\rbar$ during the last non-convergent run. Without the extra $\rbar$ term lowering the magnitude of the weight updates as in eq.(2), the network dynamics rapidly became unstable.

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.35]{chart}
\end{center}
\end{figure}
\newpage

\subsection{Time-Delayed XOR using the First Update Rule, with Unit Rewards}
This experiment was the same as the first, but issued a non-continuous reward

\[
R =
   \left\{
\begin{array}{ll}
      1 & |r_o(t) - target| = 0 \\
      0 & 0 < |r_o(t) - target| < 0.5 \\
      -1 & |r_o(t) - target| \geq 0.5 \\
\end{array} 
\right. \]

In order to prevent $(R - \rbar)$ from vanishing in eq.(2) due to a stagnant reward value, the maximum perturbation size was increased to 1 so that the network would tend to find positive and negative reward spikes fairly often. While perturbations similar to those in the other experiments had failed to produce learning, the larger perturbations had some success:
\begin{verbatim}
T_success:
10000, 10000, 10000, 10000, 10000, 10000, 10000, 2134, 10000, 10000,
10000, 10000, 10000, 10000, 10000, 10000, 10000, 5882, 10000, 10000
Median T_success: 10000.0
25th percentile T_success: 10000.0
75th percentile T_success: 10000.0
Average -R after training: 0.391572499269
\end{verbatim}

Though it was never good enough to meed the criterion for $T_{success}$ (95 consecutive trials with the correct output sign), the average reward at the end of the training runs was significantly better than random chance.

\subsection{Learning a one-hot encoding for the XOR problem trial types.}
This experiment had the same inputs as the XOR problem, but with four output neurons instead of one. We hoped to train four neurons to label the four different XOR input cases: $(0,0), (0,1), (1,0), (1,1)$. These neurons could then be used as a selector function when updating $\rbar$ in the previous experiments, rather than tracking a separate $\rbar$ for each trial type in code, which is not very biological.
Here, we randomly initialized $\rbar$, then used it to weight its own updates.
$$
\rbar_i(t) = (1 - \beta I(i))*\rbar_i(t-1) + \beta * I(i)* R(t)
$$
where $\beta = 1 - \alpha_R$ and $I(i)$ was tried as both the i-th element of the output 4-tuple $o_i$ and as $softmax(o)_i$

Unfortunately neither was very successful. The output vector sometimes aligned correctly with a couple inputs, but never converged on the correct encoding of the inputs. In principle this problem shouldn't be harder to learn than XOR, so it should be possible with some further work.
\newpage
\section{Code Performance Optimizations}

Making the network simulation run quickly was critical. Repeated attempts with large numbers of trials were required for diagnosing bugs and trying out new methods. Below are a few useful performance optimizations.
\begin{itemize}
\item The most important: Implement the eligibility update as an outer product, with S applied to the arguments instead of the product (assuming you use an S such that the result is the same, as Miconi does). This alone leads to orders of magnitude faster trials.
\item Generate the random perturbations for every timestep of a trial at the beginning of the trial. Generating random vectors at each time step is slow.
\item Load and store the input ahead of time. Inputs can even be re-used across trials, if there are only a few trial types. (as there are in the XOR problem)

My code is available on bitbucket: https://bitbucket.org/ironclownfish/recurrentnn/src
\end{itemize}

\printbibliography
\end{document}