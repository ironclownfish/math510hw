\documentclass[12pt]{article}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{fullpage}

\newcommand{\Ex}
{
	\mathbb{E}
}

\newcommand{\Prob}
{
	\mathbb{P}
}

\newcommand{\nProb}
{
	\overline{\Prob}
}

\newcommand{\deriv}[2][]
{
	\frac{\mathrm{d}#1}{\mathrm{d}#2}
}

\newcommand{\twoderiv}[2][]
{
	\frac{\mathrm{d}^2#1}{\mathrm{d}#2^2}
}

\newcounter{ProblemCounter}
\newcommand{\Problem}
{
	\stepcounter{ProblemCounter}
	\section*{Problem \arabic{ProblemCounter}}
}

\newcounter{ProblemPartCounter}[ProblemCounter]
\newcommand{\ProblemPart}
{
	\stepcounter{ProblemPartCounter}
	\subsection*{\Alph{ProblemPartCounter}.}
}

\begin{document}
\title{Homework 2}
\author{Nicholas Gyd\'e}
\maketitle

\Problem
For future reference, let the HBP comprise component Bernoulli variables $X_0, X_1, X_2,...,X_N$\\ in chronological order. Since we are allowing arbitrarily large time values (any time value in $\mathbb{N}$ is allowed), it will be necessary to take the limit $N \to \infty$ later on.

\ProblemPart
Let time $t$ be the start of an ISI ($X_{t-1} = 1$). The ISI has length $l$ if the next $l$ outcomes in the HBP are zeroes, followed by a 1. This has probability

\begin{equation*}
\Prob(ISI_t = l) = \Prob(\neg X_t, \neg X_{t+1},..., \neg X_{t+l}, X_{t+l+1})
\end{equation*}

Consider two ISIs with start times denoted $t_0$ and $t_1$. The joint distribution of their lengths is

\begin{equation*}
\Prob(ISI_{t_0} = l_0, ISI_{t_1} = l_1) = \Prob(\neg X_{t_0},..., \neg X_{t_0+l_0}, X_{t_0+l_0+1}, \neg X_{t_1},...,\neg X_{t_1+l_1}, X_{t_1+l_1+1})
\end{equation*}

No Bernoulli event takes place during more than one ISI, so all the Bernoulli variables in this joint distribution are distinct and therefore independent. Thus, we can rewrite the joint distribution

$$
\Prob(\neg X_{t_0},..., \neg X_{t_0+l_0}, X_{t_0+l_0+1})\Prob(\neg X_{t_1},...,\neg X_{t_1+l_1}, X_{t_1+l_1+1}) = \Prob(ISI_{t_0} = l_0)\Prob(ISI_{t_1} = l_1)
$$

This satisfies the definition of independence for the lengths of the two ISIs.

\ProblemPart

Using the independence of the component Bernoulli variables
$$
\Prob(ISI_t = l) = \Prob(\neg X_t, \neg X_{t+1},..., \neg X_{t+l}, X_{t+l+1}) = \Prob(X_{t+l+1})\prod_{k=t}^{t+l}\Prob(\neg X_k) = p(1 - p)^l
$$

\clearpage
\ProblemPart

\newcommand{\sumLs}[1][N]
{
	\sum_{l = 0}^{#1}
}

If $p = 0$, all r.v.s in the HBP will be 0. In that case, discussion of ISIs is ill-defined.\\
If $p \neq 0$,
\begin{align*}
\Ex[ISI] & = \sumLs\Prob(l)l = \sumLs p(1-p)^ll\\
& = p(1 - p) \sumLs l(1-p)^{l-1}\\
& = p(1 - p) \sumLs -\deriv{p}(1 - p)^l\\
& = -p(l - p) \deriv{p} \sumLs (1 - p)^l\\
& = -p(1 - p) \deriv{p} \frac{\Big(  1 - (1 - p)^{N + 1}  \Big)}{ 1 - (1 - p) }\\
& = -p(1 - p) \Bigg( \frac{Np(1-p)^N + (1-p)^N - 1}{p^2} \Bigg)\\
\end{align*}

This expression becomes much simpler in the long HBP limit. (This problem does concern an infinitely long HBP because we allow any time value in $\mathbb{N}$)

\begin{align*}
& \lim_{N \to \infty} -p(1 - p) \Bigg( \frac{Np(1-p)^N + (1-p)^N - 1}{p^2} \Bigg)\\
& =  \frac{-p(1 - p)}{p^2} \lim_{N \to \infty} \Bigg(Np(1-p)^N + (1-p)^N - 1 \Bigg)\\
& = \frac{-p(1 - p)}{p^2} \Bigg(0 + 0 - 1\Bigg)\\
& = \frac{1 - p}{p}
\end{align*}

\clearpage
Next we calculate another quantity to use in the variance. Let $N \to \infty$ from the beginning to keep the differentiation step simple.

\begin{align*}
\Ex[ISI^2] & = \sumLs[\infty]\Prob(l)l^2\\
& = p\sumLs[\infty] l^2(1-p)^l\\
& = p\sumLs[\infty] l(l-1 + 1)(1-p)^l\\
& = p\sumLs[\infty] l(l-1)(1-p)^l + p\sumLs[\infty] (1-p)^l\\
& = p(1 - p)^2 \sumLs[\infty] \twoderiv{p}(1 - p)^l + p\sumLs[\infty] (1-p)^l\\
& = p(1 - p)^2 \twoderiv{p}\sumLs[\infty] (1 - p)^l + 1\\
& = p(1 - p)^2\twoderiv{p}\frac{1}{p} + 1\\
& = p(1 - p)^2 2p^{-3} + 1\\
& = \frac{2(1-p)^2}{p^2} + 1
\end{align*}

We can now use this result along with the mean squared to compute the variance (in the $N \to \infty$ limit).

\begin{align*}
Var[ISI] & = \Ex[ISI^2] - \Ex[ISI]^2\\
& = \frac{2(1-p)^2}{p^2} + 1 - \Big(\frac{1 - p}{p}\Big)^2\\
& = \Big(\frac{1 - p}{p}\Big)^2 + 1
\end{align*}

Now we can square root this to get the standard deviation.

$$
\sigma = \Bigg(\Big(\frac{1 - p}{p}\Big)^2 + 1\Bigg)^{\frac{1}{2}}
$$

\clearpage

The coefficient of variation is

\begin{align*}
CV & = \frac{\sigma}{\Ex[ISI]}\\
& = \frac{\Bigg(\Big(\frac{1 - p}{p}\Big)^2 + 1\Bigg)^{\frac{1}{2}}}{\frac{1 - p}{p}}\\
& = \Bigg(\Big(\frac{p}{1-p}\Big)^2\Bigg)^\frac{1}{2} \Bigg(\Big(\frac{1 - p}{p}\Big)^2 + 1\Bigg)^{\frac{1}{2}}\\
& = \Bigg(\Big(\frac{p}{1-p}\Big)^2\Big(\frac{1-p}{p}\Big)^2 + 1\Bigg)^\frac{1}{2}\\
& = \sqrt{2}
\end{align*}

\Problem

\newcommand{\avg}[1]
{
	\overline{#1}
}

Start by rearranging the definition of covariance.

\begin{align*}
Cov[X,Y] &= \Ex\big[\big(X - \avg{X}\big)\big(Y - \avg{Y}\big)\big]\\
& = \Ex\big[XY - X\avg{Y} - Y\avg{X} + \avg{X}\avg{Y}\big]\\
& = \Ex\big[XY\big] - \Ex\big[X\avg{Y}\big] - \Ex\big[Y\avg{X}\big] + \Ex\big[\avg{X}\avg{Y}\big] && \text{by the linearity of $\Ex$}\\
& = \Ex\big[XY\big] - \avg{Y}\Ex\big[X\big] - \avg{X}\Ex\big[Y\big] + \avg{X}\avg{Y} && \text{using linearity to take out constants}\\
& = \Ex\big[XY\big] - 2\avg{X}\avg{Y} + \avg{X}\avg{Y}\\
& = \Ex\big[XY\big] - \avg{X}\avg{Y}\\
\end{align*}

\noindent This expression can now be plugged into the variance of $X + Y$.

\begin{align*}
Var[X + Y] & = \Ex\Big[(X + Y)^2\Big] - \Ex[X + Y]^2\\
& = \Ex[X^2 + 2XY + Y^2] - \big(\Ex[X + Y]\big)^2\\
& = \Ex[X^2] + 2\Ex[XY] + \Ex[Y^2] - \big(\Ex[X] + \Ex[Y]\big)^2 && \text{by the linearity of }\Ex\\
& = \Ex[X^2] + 2\Ex[XY] + \Ex[Y^2] - \big(\Ex[X]^2 + 2\Ex[X]\Ex[Y] + \Ex[Y]^2\big)\\
& = \Ex[X^2] - \Ex[X]^2 + \Ex[Y^2] - \Ex[Y]^2 + 2\big(\Ex[XY] - \Ex[X]\Ex[Y]\big)\\
& = Var[X] + Var[Y] + 2\big(\Ex[XY] - \avg{X}\avg{Y}\big)\\
& = Var[X] + Var[Y] + 2\big(Cov[X,Y]\big) && \parbox[t]{3.5cm}{by substituting the result above}
\end{align*}

\clearpage
\Problem
\ProblemPart

We can move the origin without affecting the value of the integral because the integral spans the entire real number line. Start by moving the origin to $x$. (i.e. translating the integrand to the right by $x$)

$$
\int_{-\infty}^{\infty} f(x - y)g(y) \mathrm{d}y = \int_{-\infty}^{\infty} f(-y)g(y - x) \mathrm{d}y 
$$

\noindent We can also reflect the integrand horizontally without changing the area under it. Thus,

$$
\int_{-\infty}^{\infty} f(-y)g(y - x) \mathrm{d}y = \int_{-\infty}^{\infty} f(y)g(x - y) \mathrm{d}y 
$$

\ProblemPart

From the rule given by equation (6) in the homework, we know

$$
\Prob_Z(x) = \int_{-\infty}^{\infty} \Prob_{Z|X}(z|x)\Prob_{X}(x)\mathrm{d}x
$$

The probability of a particular $z$, given $x$, is the probability of $Y$ taking the value $y$ such that $z = x + y$. In other words, $\Prob_{Z|X}(z|x) = \Prob_{Y}(z - x).$ Therefore,

\begin{align*}
\int_{-\infty}^{\infty} \Prob_{Z|X}(z|x)\Prob_{X}(x)\mathrm{d}x & = \int_{-\infty}^{\infty} \Prob_{Y}(z - x)\Prob_{X}(x)\mathrm{d}x\\
& = \int_{-\infty}^{\infty} g(z - x)f(x)\mathrm{d}x\\
& = (f \ast g)(z)
\end{align*}

\Problem
\ProblemPart
\begin{align*}
H[X,Y] & = -\sum_{x, y} \Prob_{X,Y}(x,y)\log \Prob_{X,Y}(x, y)\\
& = -\sum_{x, y} \Prob_{X|Y}(x|y)\Prob_Y(y) \log\Big( \Prob_{X|Y}(x|y)\Prob_Y(y) \Big) && \text{by the chain rule}\\
& = -\sum_y \Prob_Y(y)\sum_x \Big(\Prob_{X|Y}(x|y)\log\Prob_{X|Y}(x|y) + \Prob_{X|Y}(x|y)\log\Prob_Y(y) \Big)\\
& = -\Big\langle \sum_x\Prob_{X|Y}(x|y)\log\Prob_{X|Y}(x|y)\Big\rangle_Y -\Big\langle\log\Prob_Y(y) \sum_x \Prob_{X|Y}(x|y)\Big\rangle_Y\\
& = H[X|Y] - \Big\langle \log\Prob_Y(y) (1) \Big\rangle_Y && \text{because }\sum_x\Prob(x|y) = 1\\
& = H[X|Y] + H[Y]\\
\end{align*}
Instead using $\Prob(x, y) = \Prob(y|x)\Prob(x)$ in the chain rule step yields $H[X,Y] = H[Y|X] + H[X]$.
\ProblemPart
\begin{align*}
I(X;Y) & = H[X] - H[X|Y]\\
& = H[X,Y] - H[Y|X] - H[X|Y] \quad \quad \text{(by part A.)}\\
& = H[X,Y] + \bigg\langle \sum_y \Prob_{Y|X}(y|x) \log\Prob_{Y|X}(y|x) \bigg\rangle_X + \bigg\langle \sum_x \Prob_{X|Y}(x|y) \log\Prob_{X|Y}(x|y) \bigg\rangle_Y\\
& = H[X,Y] + \sum_x \Prob_X(x)\sum_y \Prob_{Y|X}(y|x) \log\Prob_{Y|X}(y|x) + \sum_y \Prob_Y(y) \sum_x \Prob_{X|Y}(x|y) \log\Prob_{X|Y}(x|y)\\
& = -\sum_{x, y} \Prob_{X,Y}(x, y)\log\Prob_{X,Y}(x, y) + \sum_{x, y} \Prob_{X,Y}(x,y)\Big( \log\Prob_{Y|X}(y|x) + \log\Prob_{X|Y}(x|y) \Big)\\
& = \sum_{x, y} \Prob_{X,Y}(x,y)\log\bigg( \frac{\Prob_{Y|X}(y|x)\Prob_{X|Y}(x|y)}{\Prob_{X,Y}(x,y)} \bigg)\\
& = \sum_{x, y} \Prob_{X,Y}(x,y)\log\bigg( \frac{\Prob_{Y|X}(y|x)\Prob_{X|Y}(x|y)}{\Prob_{Y|X}(y|x)P_X(x)} \bigg)\\
& = \sum_{x, y} \Prob_{X,Y}(x,y) \log\bigg(\frac{\Prob_{X|Y}(x|y)}{\Prob_X(x)}\bigg)\\
\end{align*}
We could also multiply by $\frac{\Prob_Y(y)}{\Prob_Y(y)}$ inside the log to get $\sum_{x,y}\Prob_{X,Y}(x,y)\log\frac{\Prob_{X,Y}(x,y)}{\Prob_X(x)\Prob_Y(y)}.$

\Problem
\ProblemPart

\newcommand{\gausN}
{
	\mathcal{N}
}
\newcommand{\gausCo}
{
	\frac{1}{\sqrt{2\pi\sigma^2}}
}
\newcommand{\ngausCo}
{
	\frac{-1}{\sqrt{2\pi\sigma^2}}
}
\newcommand{\gausExpTwo}
{
	\frac{(x - \mu)^2}{\sigma^2}
}
\newcommand{\gausExp}
{
	{-\frac{1}{2}\gausExpTwo}
}
\newcommand{\gausE}
{
	e^\gausExp
}
\newcommand{\fullGaus}
{
	\gausCo \gausE
}

\begin{align*}
H[\gausN(x)] & = -\int_{-\infty}^{\infty} \gausN(x)\log\big(\gausN(x)\big) \mathrm{d}x\\
& = -\int_{-\infty}^{\infty} \fullGaus \log\bigg(  \fullGaus  \bigg) \mathrm{d}x\\
& = - \int_{-\infty}^{\infty} \fullGaus \bigg(\log\gausCo + \log\gausE  \bigg) \mathrm{d}x\\
& = - \int_{-\infty}^{\infty} \fullGaus \bigg(-\frac{1}{2} \log(2\pi\sigma^2) \gausExp \log e  \bigg) \mathrm{d}x\\
& = \frac{1}{2} \bigg(\log(2\pi\sigma^2) \int_{-\infty}^{\infty}\fullGaus\mathrm{d}x + \frac{\log e}{\sigma^2} \int_{-\infty}^{\infty}(x - \mu)^2 \fullGaus\mathrm{d}x\bigg)\\
& = \frac{1}{2} \bigg(\log(2\pi\sigma^2) \int_{-\infty}^{\infty}\gausN(x)\mathrm{d}x + \frac{\log e}{\sigma^2} \int_{-\infty}^{\infty} (x - \mu)^2 \gausN(x) \bigg)
\end{align*}

Since $\gausN(x)$ is a probability distribution, $\int_{-\infty}^{\infty}\gausN(x) = 1$. The second integral is the definition of the variance of $\gausN(x)$, and the variance of a Gaussian distribution is $\sigma^2$. Now we have
$$
\frac{1}{2} \bigg(\log(2\pi\sigma^2) (1) + \frac{\log e}{\sigma^2} \sigma^2 \bigg) = \frac{1}{2}\log 2\pi\sigma^2e
$$

\ProblemPart

Assume the signal is independent from the noise. Then
$$
Var[X] = Var[Y] + Var[U] = \sigma_s^2 + \sigma_n^2
$$
Since $U$ and $Y$ are independent, the distribution of $U$ given $Y$ is the same as the prior distribution over $U$.
$$
Var[X|Y] = Var[Y|Y] + Var[U|Y] = 0 + \sigma_n^2
$$
We can use these to find the mutual information.
\begin{align*}
I(X;Y) & = H[X] - H[X|Y]\\
& = \frac{1}{2}\log 2\pi(\sigma_s^2 + \sigma_n^2) - \frac{1}{2}\log 2\pi(\sigma_n^2)\\
& = \log \frac{\sqrt{\sigma_s^2 + \sigma_n^2}}{\sigma_n}\\
\end{align*}

\end{document}